{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assignment 1: Classification\n",
    "\n",
    "Machine Learning for Finance  \n",
    "VU Amsterdam    \n",
    "\n",
    "**Instructors**  \n",
    "Iman van Lelyveld (iman.van.lelyveld@vu.nl)  \n",
    "Dieter Wang (d.wang@vu.nl)  \n",
    "\n",
    "<span style='color:crimson; font-weight: bold'>Submission deadline: 13 Nov 2019, 6pm CET</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instructions\n",
    "* Each group submits _only one_ notebook via canvas on the assignment page.  \n",
    "Not a zip folder, not the data, only one notebook file.\n",
    "* The notebook should be named `assignment1_groupXX.ipynb` where `XX` is your group number,  \n",
    "e.g. for group 3 this will be `assignment1_group03.ipynb`.  \n",
    "* The notebook should run without raising any errors.\n",
    "* We recommend the folder structure\n",
    "```\n",
    "assignment/\n",
    "-- 1_data/\n",
    "-- 2_code/\n",
    "-- 3_results/\n",
    "-- assignment1_groupXX.ipynb\n",
    "```\n",
    "* We strongly recommend git, as you are encouraged to collaborate and split up the work and maybe even start independently. To see how to set up your own repo for your group, see `L2-git-slides.pdf`, slides 20 onwards.\n",
    "* Do not spend time on optimizing the speed of your code.\n",
    "* We strongly encourage you to experiment, try different approaches and combinations and get to know the problem from alternative angles. But the final notebook should only contain the necessary results for grading."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What determines sovereign credit ratings?\n",
    "\n",
    "In this assignment you will classify country ratings based on observable charateristics. We use the Standard & Poor's Long-term bond ratings (AAA+ to D) for 108 countries. We do not use them directly but instead transform them into [EU Credit Quality Step](https://en.wikipedia.org/wiki/Credit_rating#Corporate_credit_ratings) ranging from 1 (AAA+) to 6 (D). This is stored in the column `cqs`. To facilitate the classification task, we further subdivide them into \n",
    "* `cqs_group3` has the values `rating_high=1`,`rating_medium=2` and `rating_low=3`\n",
    "* `cqs_group2` has the values `rating_high=1` and `rating_low=2`\n",
    "\n",
    "You can inspect the data to find out how `cqs`, `cqs_group2` and `cqs_group3` relate to each other.\n",
    "\n",
    "To classify the countries, you will find the seven following features. For variable descriptions, you can visit [World Bank's World Development Indicators](http://datatopics.worldbank.org/world-development-indicators/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_features = {\n",
    "    'co2':            'CO2 emissions (metric tons per capita)',\n",
    "    'doctors':        'Physicians (per 1,000 people)',\n",
    "    'urban_pop':      'Urban population (% of total)',\n",
    "    'cellular':       'Mobile cellular subscriptions (per 100 people)',\n",
    "    'deaths_infant':  'Mortality rate, under-5 (per 1,000 live births)',\n",
    "    'age_dep':        'Age dependency ratio (% of working-age population)',\n",
    "    'gni_growth':     'GNI growth (annual %)',\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's load and have a look at the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = os.path.join('..','1_data','country_data.csv')\n",
    "\n",
    "df_data = pd.read_csv(path)\n",
    "df_data = df_data.set_index('iso3')\n",
    "\n",
    "df_data.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we start the analysis, we prepare the data by labelling them correctly and splitting into targets and features. We will start with a binary classification (`cqs_group2`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target_name = 'cqs_group2'\n",
    "\n",
    "# `sr_` prefix stands for a pandas Series\n",
    "sr_targets = df_data.loc[:,target_name]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These targets can take on two values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "label_values = [1,2]\n",
    "label_names = ['rating_high', 'rating_low']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The features are as described above. Let us use the short names from now on and extract those from the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "selected = list(all_features.keys())\n",
    "\n",
    "# `df_` prefix stands for a pandas DataFrame\n",
    "df_features = df_data.loc[:,selected]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 1\n",
    "**10 points**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import the function `plot_pairwise_scatter` from `lib.plots` and visualize how the variables relate to each other. The function is used as follows\n",
    "```\n",
    "plot_pairwise_scatter(sr_targets, df_features, label_values)\n",
    "```\n",
    "\n",
    "Describe the data in words. What variables do you expect to be important for the classification? You can argue using economic, financial or statistical arguments. \n",
    "\n",
    "Furthermore, do you need to pre-process your data? If so, what type of preprocessing may be appropriate or even necessary here?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 2\n",
    "**10 points**\n",
    "\n",
    "Based on your arguments in Question 1, decide for two features. These two features `feature1, feature2` will be your inputs for the subsequent classifications.\n",
    "\n",
    "Make a plot and describe what a linear classifier is likely to do. What challenges could it run into? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feature1 = ''\n",
    "feature2 = ''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 3\n",
    "**15 points**\n",
    "\n",
    "Use a linear support vector machine from the `sklearn.svm` module and plot the results. \n",
    "\n",
    "For the plot, import `plot_classification_contours` from `lib.plots`. Use it like this\n",
    "```\n",
    "plot_classification_contours(X, y, clf, [feature1,feature2])\n",
    "```\n",
    "where `clf` is the sklearn classifier object. `X` is a numpy array with `[n_obs, n_features]` dimensions and `y` is a numpy array with `[n_obs,]` dimensions.\n",
    "\n",
    "**Hint:** You can leave the default options of your classifier. But if you decide to adjust them, please give an explanation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 4\n",
    "**25 points**\n",
    "\n",
    "How well did the classification go? \n",
    "\n",
    "Make predictions and call them `y_pred`. Write four functions to compute the four elements of a confusion matrix. Name them appropriately and cross check their results with the output of the following plot\n",
    "```\n",
    "plot_confusion_matrix(y, y_pred, label_names, normalize=True)\n",
    "```\n",
    "Once again, you can import this function from `lib.plots`.\n",
    "\n",
    "Furthermore, write two functions to compute the precision and recall of your classifier. \n",
    "\n",
    "**Hint:** The functions you write should have the following structure:\n",
    "```\n",
    "def true_positive(y_true, y_pred):\n",
    "    # magic\n",
    "    return TP\n",
    "```\n",
    "It is important here that `y_true` and `y_pred` follow the same binary classifications and agree on what's \"positive\" and whats \"negative\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 5\n",
    "**15 points**\n",
    "\n",
    "Change the classifier to a radial basis function and discuss the results. Does it do better than the linear classifier? If so, in what respect? If not, why? Is the confusion matrix different?\n",
    "\n",
    "**Hint:** In sklearn, the `gamma` parameter may need some adjustment, depending on your features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 6\n",
    "**15 points**\n",
    "\n",
    "Let us know consider a third feature `feature3` as an additional predictor variable. Look at the previous pairplot for this purpose and identify a third input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feature3 = ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can again visualize the three features using \n",
    "```\n",
    "selected = [feature1,feature2,feature3]\n",
    "plot_pairwise_scatter(sr_targets, df_features.loc[:,selected], label_values)\n",
    "```\n",
    "\n",
    "Use this new feature and rerun the classification. Does the new feature improve the predictions, or is it possible that it does worse? Please explain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 7\n",
    "**10 points**\n",
    "\n",
    "Finally, let us switch from a binary classification to a classification of three outcomes. This corresponds to the columns `cqs_group3`, where we have `rating_high`, `rating_medium` and `rating_low`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target_name = 'cqs_group3'\n",
    "\n",
    "sr_targets = df_data.loc[:,target_name]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "label_values = [1,2,3]\n",
    "label_names = ['rating_high', 'rating_mid', 'rating_low']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can again use the \n",
    "```\n",
    "selected = [feature1,feature2,feature3]\n",
    "plot_pairwise_scatter(sr_targets, df_features.loc[:,selected], label_values)\n",
    "```\n",
    "command to visualize your data.\n",
    "\n",
    "Inspect the classification results. Can you use the functions you defined previously to asses the results, or do you need to make adjustments? If so, do you have to completely rewrite the functions or are only minor adjustments necessary?\n",
    "\n",
    "## Bonus question\n",
    "**10 points**\n",
    "Compute the confusion matrix.  \n",
    "Compute precision and recall for this case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Your answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
